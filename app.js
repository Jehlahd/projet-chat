/* Initialisation du serveur local */

const express = require("express");
const app = express();
const http = require("http").createServer(app);
const port = 2552;
const bodyParser = require("body-parser");
const io = require("socket.io")(http);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static("./"));

app.get("/", (req, res) => res.sendFile("index.html"));

io.on("connection", function(socket) {
  console.log("a user connected");
  socket.on("disconnect", function() {
    console.log("user disconnected");
  });
  socket.on('chat message', function(msg){
    io.emit('chat message', msg);
  });
  
});

io.emit('some event', { for: 'everyone' });

http.listen(port, () => console.log(`Example app listening on port ${port}!`));

/* Création du chat */
